import { asapScheduler } from "rxjs";
import { IPokemon } from "./Pokemon";
const API_URL = "https://srh-rest-api-2.herokuapp.com/trainers"
const API_KEY = "NIqabBeHBlpINB8pPqrvuaS3Du9SI_oGd_AJ4mtGsspSiU_BXe5o_uW_bNZuPS1wIWvJZfaSfPk2RBDxlED5mQ";
export const nullUser = { id: NaN, username: "", pokemon: [] };
export async function login(username: string, successfulCallback: (user: IUser) => void, errorCallback: (error: any) => void) {
    let user: IUser = nullUser;
    try {
        const users = await getUsersWithSimilarName(username);
        if (users === undefined || users.length === 0) {
            user = await registerNewUser(username);
        }
        else {
            user = users[0];
        }
    }
    catch (error) {
        errorCallback(error)
        return;
    }
    successfulCallback(user);

}

export async function addPokemon(username: string, pokemon: IPokemon[], callback: (user: IUser) => void) {
    const users = await getUsersWithSimilarName(username);
    if (users.length == 0) {
        throw new Error("Tried to add a pokemon to a non-existing user. Username '" + username + "' has no entries in the database.");
    }
    const user: IUser = users[0];
    const previousPokemon: IPokemon[] = user.pokemon;
    const pokemonSum: IPokemon[] = [...previousPokemon, ...pokemon];
    await setOwnedPokemon(username, pokemonSum, ()=>{});
    const output: IUser = {id: user.id, username: username, pokemon: pokemonSum};
    callback(output);
}
export async function getOwnedPokemon(username: string, callback: (pokemon: string[]) => void) {
    const users = await getUsersWithSimilarName(username);
    if (users.length == 0) {
        throw new Error("Username '" + username + "' has no entries in the database.");
    }
    callback(users[0].pokemon);
    return users[0].pokemon;
}

async function setOwnedPokemon(username: string, pokemon: IPokemon[], callback: () => void) {
    const requestOptions = {
        method: "PATCH",

        headers: {
            "X-API-KEY": API_KEY,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: username,
            pokemon: pokemon
        })
    }
    const users = await getUsersWithSimilarName(username)
    const id = users[0].id;
    const registerEndpoint = API_URL + "/" + id;
    const output = await (fetch(registerEndpoint, requestOptions)
        .then(response => response.json())
        .then(data => data.data)
    )
    if (callback !== undefined) {
        callback();
    }
}

async function getUsersWithSimilarName(username: string) {
    const users = await fetch(API_URL + "?username=" + username)
        .then(response => response.json())
        .catch(error => console.error(error))
    return users;
}
async function registerNewUser(username: string) {
    // Creates a new blank user. NB: Does not check whether the user already exists.
    console.log("Registering user '" + username + "'")
    const requestOptions = {
        method: "POST",

        headers: {
            "X-API-KEY": API_KEY,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: username,
            pokemon: []
        })
    }
    const registerEndpoint = API_URL;
    const response = await fetch(registerEndpoint, requestOptions);
    const data = await response.json();
    return data;
}
export interface IUser {
    id: number;
    username: string;
    pokemon: IPokemon[];
}