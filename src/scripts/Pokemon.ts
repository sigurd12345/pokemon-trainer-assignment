export interface IPage {
    results: IPokemon[]
    next: string;
}

export interface IPokemon {
    id: number;
    name: string;
    url: string;
    
}
export function getImageURL(pokemon: IPokemon){
  return imageUrl(getIdFromUrl(pokemon.url));
}

function getIdFromUrl(url: string): string {
    let split: string[] = url.split("/");
    if (split.length < 2) {
      throw new Error("this url is undefined or incorrect")
    }

    return split[split.length - 2];
  }
function imageUrl(id: string | number): string {
    return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png";
  }