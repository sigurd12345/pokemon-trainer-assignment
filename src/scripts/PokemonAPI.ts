
const baseURL: string = "https://pokeapi.co/api/v2/pokemon/";
import { IPage, IPokemon } from "src/scripts/Pokemon";

export const PokemonAPI = {
    async getFirstPage(callback: (page: IPage)=>void) {
        const page: IPage = await fetch(baseURL)
            .then(response => response.json())
            .catch(error => console.error(error));

            
        callback(page);
    }
}