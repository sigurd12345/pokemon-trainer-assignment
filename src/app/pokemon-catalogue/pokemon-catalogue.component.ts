import { Component, OnInit } from '@angular/core';
import { PokemonAPI } from 'src/scripts/PokemonAPI';
import { IPage, IPokemon, getImageURL } from 'src/scripts/Pokemon';
import { IUser, login, addPokemon, nullUser } from 'src/scripts/LoginAPI';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {
  public user: IUser = nullUser;
  public pokemon: IPokemon[] = [];
  public imageURLs: string[] = [];
  public pokemonIsOwned: boolean[] = [];
  public setupIsComplete = false;
  
  constructor(private router: Router) { }

  ngOnInit(): void {
    let temp = localStorage.getItem("user");

    if (temp !== null) {
      this.user = JSON.parse(temp);
      if (this.user.username === "") {
        this.router.navigate([""]);
      }
    }
    else {
      this.router.navigate([""]);
    }
    PokemonAPI.getFirstPage((page: IPage) => this.handleFirstPage(page));
  }
  handleFirstPage(page: IPage) {
    this.pokemon = page.results;
    this.imageURLs = this.pokemon.map((p: IPokemon) => getImageURL(p));
    this.updateOwnedPokemon(this.user.pokemon.map((pokemon) => pokemon.name));
  }


  clickPokemon(index: number) {
    console.log("Clicked on " + this.pokemon[index].name);
    // Add pokemon to local version of the user
    this.user.pokemon.push(this.pokemon[index]);
    this.setUserData(this.user);
    //Add pokemon to remote-version of user. The callback will ensure that the remote and local are in sync.
    addPokemon(this.user.username, [this.pokemon[index]], (user) => this.setUserData(user))
    this.pokemonIsOwned[index] = true;
  }

  setUserData(user: IUser) {
    if (user === undefined || user.id === undefined || user.id === NaN){
      return;
    }
    this.user = user;
    localStorage.setItem("user", JSON.stringify(user));
    this.updateOwnedPokemon(user.pokemon.map((pokemon) => pokemon.name));
  }

  updateOwnedPokemon(pokemonNames: string[]) {
    this.pokemonIsOwned = new Array(this.pokemon.length);
    for (let i = 0; i < this.pokemon.length; i++) {
      this.pokemonIsOwned[i] = pokemonNames.includes(this.pokemon[i].name);
    }
    this.setupIsComplete = true;
  }

  navigateToTrainerPage() {
    this.router.navigate(["trainer"]);
  }

  logout() {
    localStorage.setItem("user", JSON.stringify(nullUser));
    this.router.navigate([""]);
  }
}
