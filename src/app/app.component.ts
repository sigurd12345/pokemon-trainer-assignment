import { Component } from '@angular/core';
import { IUser } from 'src/scripts/LoginAPI';
import { nullUser } from 'src/scripts/LoginAPI';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokemon-trainer';

  user: IUser = nullUser;
  handleLoginComplete(user: IUser) {
    this.user = user;
  }

}
