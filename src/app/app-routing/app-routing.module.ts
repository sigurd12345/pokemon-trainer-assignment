import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from '../landing-page/landing-page.component';
import { TrainerPageComponent } from '../trainer-page/trainer-page.component';
import { PokemonCatalogueComponent } from '../pokemon-catalogue/pokemon-catalogue.component';

const routes: Routes = [
  {
    path: "",
    component: LandingPageComponent
  },
  {
    path: "trainer",
    component: TrainerPageComponent
  },
  {
    path: "catalogue",
    component: PokemonCatalogueComponent
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
