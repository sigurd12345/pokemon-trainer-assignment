import { Component, OnInit } from '@angular/core';
import { login, IUser, nullUser } from 'src/scripts/LoginAPI';
import { IPokemon, getImageURL } from 'src/scripts/Pokemon';
import { Router } from '@angular/router';
@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {
  public setupIsComplete: boolean = false;
  public user: IUser = nullUser;
  
  public imageURLs: string[] = [];
  constructor(private router: Router) { }

  ngOnInit(): void {
  
      let temp = localStorage.getItem("user");

    if(temp !== null) {
      this.user = JSON.parse(temp);
      if (this.user.username === "") {
        this.router.navigate([""]);
      }
    } else {
      this.router.navigate([""]);
    }
    this.imageURLs = this.user.pokemon.map((p: IPokemon) => getImageURL(p));
    this.setupIsComplete = true;
  }
  navigateToCatalogue(){
    this.router.navigate(["catalogue"]);
  }
  logout(){
    localStorage.setItem("user", JSON.stringify(nullUser));
    this.router.navigate([""]);
  }
}
