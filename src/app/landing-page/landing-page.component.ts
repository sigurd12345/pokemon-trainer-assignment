import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { login } from 'src/scripts/LoginAPI';
import { IUser } from 'src/scripts/LoginAPI';
import { nullUser } from 'src/scripts/LoginAPI';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private router: Router) { }
  public allowLoginClick = true; // Prevents user from sending multiple login requests simultaneously.
  
  user: IUser = nullUser;
  error: string = "";

  ngOnInit(): void {
    // If user is already logged in, redirect automatically.
    let temp = localStorage.getItem("user");
    if (temp !== null) {
      this.user = JSON.parse(temp);
      if (this.user.id !== NaN && this.user.username !=="") {
        this.router.navigate(['trainer']);
      }
    }
  }


  loginSuccess(user: IUser) {
    this.allowLoginClick = true;
    this.user = user;
    this.complete.emit(user);
    console.log("The user is: ", user);

    localStorage.setItem("user", JSON.stringify(user));
    this.router.navigate(['trainer']);
  }

  loginError(error: any) {
    this.allowLoginClick = true;
    console.error(error);
    this.error = error.message;
  }

  onLoginClick(): void {
    if (!this.allowLoginClick){
      return;
    }
    this.allowLoginClick = false;
    login(
      this.userName,
      (u) => this.loginSuccess(u),
      (err) => this.loginError(err))
      ;
  }

  userName: string = "";
  setUsernameInput(event: any): void {
    this.userName = event.target.value;
  }

  @Output() complete: EventEmitter<IUser> = new EventEmitter();

}
