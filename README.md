# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.
Heroku Link: https://agile-island-77249.herokuapp.com/

The application utilises the pokemon API from: https://pokeapi.co/ 
Lets the user catch pokemon, and view a full list of all available pokemons.

## Table of Contents

-   [Install](#install)
-   [Usage](#usage)
-   [Component-tree](#Component-tree)
-   [Maintainers](#maintainers)
-   [License](#license)

## Install

```
npm install
```

## Usage

```
Run `ng serve` to run the app on a local server. It defaults to localhost:4200
```

## Component-tree

This was the component tree we created and used as a guide for implementing our solution.

![Component tree](./angular-component-tree.png "Component tree")

## Maintainers

@sigurd12345
@Mokleiv

## License

MIT © 2022 Sigurd Riis Haugen, Sondre Mokleiv Nygård
